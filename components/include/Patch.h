#pragma once

#include "Geode.h"

class Patch :
    public Geode
{
    Vector4 points[16];
    float phase;
public:
    Patch();
    Patch(float phase);
    ~Patch();

    void update();
    void render();
};

