#include "Skybox.h"
#include "MatrixTransform.h"
#include "Texture.h"
#include "Utility.h"

static const char front[] = { "resources/front.ppm" };
static const char back[] = { "resources/back.ppm" };
static const char left[] = { "resources/left.ppm" };
static const char right[] = { "resources/right.ppm" };
static const char top[] = { "resources/top.ppm" };
static const char bottom[] = { "resources/base.ppm" };

Skybox::Skybox()
{
    MatrixTransform *transform;
    Texture *texture_group;
    Matrix4 tmp;
    
    /* Back wall */
    texture_group = new Texture();
    texture_group->loadTexture(back);
    texture_group->add_child(new Wall(5, 3));
    tmp = Matrix4(Matrix4::tTranslate, 0, 2.5, -1.5) * Matrix4(Matrix4::tRotate, 180, 0, 1, 0);
    transform = new MatrixTransform(tmp.clone());
    transform->add_child(texture_group);
    add_child(transform);

    /* Front wall */
    texture_group = new Texture();
    texture_group->loadTexture(front);
    texture_group->add_child(new Wall(5, 3));
    tmp = Matrix4(Matrix4::tTranslate, 0, 2.5, 1.5) * Matrix4(Matrix4::tRotate, 0, 0, 1, 0);
    transform = new MatrixTransform(tmp.clone());
    transform->add_child(texture_group);
    add_child(transform);

    /* Top wall */
    texture_group = new Texture();
    texture_group->loadTexture(top);
    texture_group->add_child(new Wall(3, 3));
    tmp = Matrix4(Matrix4::tTranslate, 0, 5, 0) * Matrix4(Matrix4::tRotate, -90, 1, 0, 0);
    transform = new MatrixTransform(tmp.clone());
    transform->add_child(texture_group);
    add_child(transform);

    /* Base wall */
    texture_group = new Texture();
    texture_group->loadTexture(bottom);
    texture_group->add_child(new Wall(3, 3));
    tmp = Matrix4(Matrix4::tTranslate, 0, 0, 0) * Matrix4(Matrix4::tRotate, 90, 1, 0, 0);
    transform = new MatrixTransform(tmp.clone());
    transform->add_child(texture_group);
    add_child(transform);

    /* Left wall */
    texture_group = new Texture();
    texture_group->loadTexture(left);
    texture_group->add_child(new Wall(5, 3));
    tmp = Matrix4(Matrix4::tTranslate, -1.5, 2.5, 0) * Matrix4(Matrix4::tRotate, -90, 0, 1, 0);
    transform = new MatrixTransform(tmp.clone());
    transform->add_child(texture_group);
    add_child(transform);

    /* Right wall */
    texture_group = new Texture();
    texture_group->loadTexture(right);
    texture_group->add_child(new Wall(5, 3));
    tmp = Matrix4(Matrix4::tTranslate, 1.5, 2.5, 0) * Matrix4(Matrix4::tRotate, 90, 0, 1, 0);
    transform = new MatrixTransform(tmp.clone());
    transform->add_child(texture_group);
    add_child(transform);
}

Skybox::~Skybox()
{
}

void Skybox::draw(const Matrix4& C)
{
    Group::draw(C);

    GL_DEBUG("Skybox::draw");
}

void Skybox::render()
{
    GLuint SkyBoxVBO;
    float SkyBoxVertices[] =
    {	// x, y, z, x, y, z, x, y, z, x, y, z
        1.0f, -1.0f, -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, -1.0f, // +X
        -1.0f, -1.0f, 1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, // -X
        -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f, // +Y
        -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, // -Y
        1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, // +Z
        -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f, -1.0f  // -Z
    };

    glGenBuffers(1, &SkyBoxVBO);
    glBindBuffer(GL_ARRAY_BUFFER, SkyBoxVBO);
    glBufferData(GL_ARRAY_BUFFER, 288, SkyBoxVertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

