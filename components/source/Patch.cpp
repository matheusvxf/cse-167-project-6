#include "Patch.h"
#include "Vector4.h"
#include "Matrix4.h"
#include "Config.h"
#include "utility.h"

#include <cmath>

static Matrix4 B_bez(
    Vector4(-1, 3, -3, 1),
    Vector4(3, -6, 3, 0),
    Vector4(-3, 3, 0, 0),
    Vector4(1, 0, 0, 0));
static const UINT32 n_u = 16, n_v = 16;
static Matrix4 Gx, Gy, Gz;

static Vector4 bez(float u, const Matrix4& C);

inline Matrix4 bez_make_c(const Vector4& p0, const Vector4& p1, const Vector4& p2, const Vector4& p3)
{
    static Matrix4 G_bez;

    G_bez.setColumn(0, p0);
    G_bez.setColumn(1, p1);
    G_bez.setColumn(2, p2);
    G_bez.setColumn(3, p3);

    return G_bez * B_bez;
}

inline Vector4 bez(float u, const Vector4& p0, const Vector4& p1, const Vector4& p2, const Vector4& p3)
{
    return bez(u, bez_make_c(p0, p1, p2, p3));
}

inline Vector4 bez(float u, const Matrix4& C)
{
    Vector4 T(u * u * u, u * u, u, 1);

    return C * T;
}

Patch::Patch() : Patch(0) {}

Patch::Patch(float phase) : phase(phase)
{
    update();
}

Patch::~Patch()
{
}

void Patch::update()
{
    static float t;
    static float frequency = 0.0001;

    t += 1;
    for (UINT8 i = 0; i < 16; ++i)
    {
        Gx[i / 4][i % 4] = -1.5 + i % 4;
        Gy[i / 4][i % 4] = sin(2 * M_PI * frequency * t + (2.0 * M_PI / 3.0) * (i % 4));
        Gz[i / 4][i % 4] = -1.5 + i / 4;

        points[i].set(Gx[i / 4][i % 4], Gy[i / 4][i % 4], Gz[i / 4][i % 4], 1);
    }
}

void Patch::render()
{
    static Vector4 n, x1, x2, x3, x4;
    static Vector4 tmp;
    float u1, u2, v1, v2;
    Vector4 q1[4], q2[4];
    float vertice[n_u * n_v * 12];
    UINT32 n_vertice = (n_u * n_v * 12) / 3;
    UINT32 index;
    static Matrix4 C_u[4], C_q1, C_q2;

#if(DEBUG == TRUE)
    for (register UINT8 i = 0; i < 16; ++i)
    {
        glPushMatrix();
        glTranslated(points[i][0], points[i][1], points[i][2]);
        glutSolidSphere(0.1, 16, 16);
        glPopMatrix();
    }
#endif

    for (register UINT8 i = 0; i < 4; ++i)
    {
        C_u[i] = bez_make_c(points[i * 4], points[i * 4 + 1], points[i * 4 + 2], points[i * 4 + 3]);
    }

    glNormal3b(0, 1, 0);
    glColor3b(1, 1, 1);
    for (UINT32 i = 0; i < n_u; ++i)
    {
        u1 = (float)i / (float)n_u;
        u2 = (float)(i+1) / (float)n_u;

        for (register UINT8 q_n = 0; q_n < 4; ++q_n)
        {
            q1[q_n] = bez(u1, C_u[q_n]);
            q2[q_n] = bez(u2, C_u[q_n]);
        }

        C_q1 = bez_make_c(q1[0], q1[1], q1[2], q1[3]);
        C_q2 = bez_make_c(q2[0], q2[1], q2[2], q2[3]);

        for (UINT32 j = 0; j < n_v; ++j)
        {
            v1 = (float)j / (float)n_v;
            v2 = (float)(j + 1) / (float)n_v;

            x1 = bez(v1, C_q1);
            x2 = bez(v1, C_q2);
            x3 = bez(v2, C_q1);
            x4 = bez(v2, C_q2);

            index = i * n_v * 12 + j * 12;
            vertice[index + 0] = x3[0];
            vertice[index + 1] = x3[1];
            vertice[index + 2] = x3[2];
            vertice[index + 3] = x4[0];
            vertice[index + 4] = x4[1];
            vertice[index + 5] = x4[2];
            vertice[index + 6] = x2[0];
            vertice[index + 7] = x2[1];
            vertice[index + 8] = x2[2];
            vertice[index + 9] = x1[0];
            vertice[index + 10] = x1[1];
            vertice[index + 11] = x1[2];
        }
    }

    glEnableClientState(GL_VERTEX_ARRAY);
    //glEnableClientState(GL_NORMAL_ARRAY);

    glVertexPointer(3, GL_FLOAT, 0, vertice);
    //glNormalPointer(GL_FLOAT, 0, normals);

    glDrawArrays(GL_QUADS, 0, n_vertice);

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);

    GL_DEBUG("Patch::render - end");
}
