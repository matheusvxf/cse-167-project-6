#version 120

varying vec3 normal;

void main()
{
	normal = normalize(gl_NormalMatrix * gl_Normal);
	vec4 Position = gl_ModelViewMatrix * gl_Vertex;
    vec3 Reflection = reflect(Position.xyz, normal);
    gl_TexCoord[0].stp = vec3(Reflection.x, -Reflection.yz);
	gl_Position = ftransform();
}